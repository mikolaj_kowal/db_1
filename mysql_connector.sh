#!/bin/bash
BASE_PATH=$(dirname $0)

echo "Waiting for mysql to get up"
# Give 100 seconds for master and slave to come up
sleep 100

sh $BASE_PATH/mysql_connector_1.sh
echo '----------------------------------------------------------------------------------------------------'
sh $BASE_PATH/mysql_connector_2.sh
echo '----------------------------------------------------------------------------------------------------'
sh $BASE_PATH/mysql_connector_3.sh