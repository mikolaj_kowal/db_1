#!/bin/bash
BASE_PATH=$(dirname $0)


echo "* Create replication user for pair 2"

mysql --host mysqlslave2 --port=3307 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e 'STOP SLAVE;';
mysql --host mysqlslave2 --port=3307 -uroot -p$MYSQL_MASTER_PASSWORD -AN -e 'RESET SLAVE ALL;';

mysql --host mysqlmaster2 --port=3307 -uroot -p$MYSQL_MASTER_PASSWORD -AN -e "CREATE USER '$MYSQL_REPLICATION_USER'@'%';"
mysql --host mysqlmaster2 --port=3307 -uroot -p$MYSQL_MASTER_PASSWORD -AN -e "GRANT REPLICATION SLAVE ON *.* TO '$MYSQL_REPLICATION_USER'@'%' IDENTIFIED BY '$MYSQL_REPLICATION_PASSWORD';"
mysql --host mysqlmaster2 --port=3307 -uroot -p$MYSQL_MASTER_PASSWORD -AN -e 'flush privileges;'


echo "* Set MySQL21 as master on MySQL22"

MYSQL21_Position=$(eval "mysql --host mysqlmaster2 --port=3307 -uroot -p$MYSQL_MASTER_PASSWORD -e 'show master status \G' | grep Position | sed -n -e 's/^.*: //p'")
MYSQL21_File=$(eval "mysql --host mysqlmaster2 --port=3307 -uroot -p$MYSQL_MASTER_PASSWORD -e 'show master status \G'     | grep File     | sed -n -e 's/^.*: //p'")
MASTER_IP2=$(eval "getent hosts mysqlmaster2|awk '{print \$1}'")
echo $MASTER_IP2
mysql --host mysqlslave2 --port=3307 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "CHANGE MASTER TO master_host='mysqlmaster2', master_port=3307, \
        master_user='$MYSQL_REPLICATION_USER', master_password='$MYSQL_REPLICATION_PASSWORD', master_log_file='$MYSQL21_File', \
        master_log_pos=$MYSQL21_Position;"

# echo "* Set MySQL22 as master on MySQL21"

# MYSQL22_Position=$(eval "mysql --host mysqlslave2 --port=3307 -uroot -p$MYSQL_SLAVE_PASSWORD -e 'show master status \G' | grep Position | sed -n -e 's/^.*: //p'")
# MYSQL22_File=$(eval "mysql --host mysqlslave2 --port=3307 -uroot -p$MYSQL_SLAVE_PASSWORD -e 'show master status \G'     | grep File     | sed -n -e 's/^.*: //p'")

# SLAVE_IP2=$(eval "getent hosts mysqlslave2|awk '{print \$1}'")
# echo $SLAVE_IP2
# mysql --host mysqlmaster2 --port=3307 -uroot -p$MYSQL_MASTER_PASSWORD -AN -e "CHANGE MASTER TO master_host='mysqlslave2', master_port=3307, \
#         master_user='$MYSQL_REPLICATION_USER', master_password='$MYSQL_REPLICATION_PASSWORD', master_log_file='$MYSQL22_File', \
#         master_log_pos=$MYSQL22_Position;"

echo "* Start Slave on both Servers"
mysql --host mysqlslave2 --port=3307 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "start slave;"

echo "Increase the max_connections to 2000"
mysql --host mysqlmaster2 --port=3307 -uroot -p$MYSQL_MASTER_PASSWORD -AN -e 'set GLOBAL max_connections=2000';
mysql --host mysqlslave2 --port=3307 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e 'set GLOBAL max_connections=2000';

mysql --host mysqlslave2 --port=3307 -uroot -p$MYSQL_MASTER_PASSWORD -e "show slave status \G"

echo "MySQL servers created!"
echo "--------------------"
echo
echo Variables available fo you :-
echo
echo MYSQL21_IP       : mysqlmaster2
echo MYSQL22_IP       : mysqlslave2