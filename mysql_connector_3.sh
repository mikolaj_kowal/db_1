#!/bin/bash
BASE_PATH=$(dirname $0)


echo "* Create replication user for pair 3"

mysql --host mysqlslave3 --port=3308 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e 'STOP SLAVE;';
mysql --host mysqlslave3 --port=3308 -uroot -p$MYSQL_MASTER_PASSWORD -AN -e 'RESET SLAVE ALL;';

mysql --host mysqlmaster3 --port=3308 -uroot -p$MYSQL_MASTER_PASSWORD -AN -e "CREATE USER '$MYSQL_REPLICATION_USER'@'%';"
mysql --host mysqlmaster3 --port=3308 -uroot -p$MYSQL_MASTER_PASSWORD -AN -e "GRANT REPLICATION SLAVE ON *.* TO '$MYSQL_REPLICATION_USER'@'%' IDENTIFIED BY '$MYSQL_REPLICATION_PASSWORD';"
mysql --host mysqlmaster3 --port=3308 -uroot -p$MYSQL_MASTER_PASSWORD -AN -e 'flush privileges;'


echo "* Set MySQL31 as master on MySQL32"

MYSQL31_Position=$(eval "mysql --host mysqlmaster3 --port=3308 -uroot -p$MYSQL_MASTER_PASSWORD -e 'show master status \G' | grep Position | sed -n -e 's/^.*: //p'")
MYSQL31_File=$(eval "mysql --host mysqlmaster3 --port=3308 -uroot -p$MYSQL_MASTER_PASSWORD -e 'show master status \G'     | grep File     | sed -n -e 's/^.*: //p'")
MASTER_IP3=$(eval "getent hosts mysqlmaster3|awk '{print \$1}'")
echo $MASTER_IP3
mysql --host mysqlslave3 --port=3308 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "CHANGE MASTER TO master_host='mysqlmaster3', master_port=3308, \
        master_user='$MYSQL_REPLICATION_USER', master_password='$MYSQL_REPLICATION_PASSWORD', master_log_file='$MYSQL31_File', \
        master_log_pos=$MYSQL31_Position;"

# echo "* Set MySQL32 as master on MySQL31"

# MYSQL32_Position=$(eval "mysql --host mysqlslave3 --port=3308 -uroot -p$MYSQL_SLAVE_PASSWORD -e 'show master status \G' | grep Position | sed -n -e 's/^.*: //p'")
# MYSQL32_File=$(eval "mysql --host mysqlslave3 --port=3308 -uroot -p$MYSQL_SLAVE_PASSWORD -e 'show master status \G'     | grep File     | sed -n -e 's/^.*: //p'")

# SLAVE_IP3=$(eval "getent hosts mysqlslave3|awk '{print \$1}'")
# echo $SLAVE_IP3
# mysql --host mysqlmaster3 --port=3308 -uroot -p$MYSQL_MASTER_PASSWORD -AN -e "CHANGE MASTER TO master_host='mysqlslave3', master_port=3308, \
#         master_user='$MYSQL_REPLICATION_USER', master_password='$MYSQL_REPLICATION_PASSWORD', master_log_file='$MYSQL32_File', \
#         master_log_pos=$MYSQL32_Position;"

echo "* Start Slave on both Servers"
mysql --host mysqlslave3 --port=3308 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "start slave;"

echo "Increase the max_connections to 2000"
mysql --host mysqlmaster3 --port=3308 -uroot -p$MYSQL_MASTER_PASSWORD -AN -e 'set GLOBAL max_connections=2000';
mysql --host mysqlslave3 --port=3308 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e 'set GLOBAL max_connections=2000';

mysql --host mysqlslave3 --port=3308 -uroot -p$MYSQL_MASTER_PASSWORD -e "show slave status \G"

echo "MySQL servers created!"
echo "--------------------"
echo
echo Variables available fo you :-
echo
echo MYSQL31_IP       : mysqlmaster3
echo MYSQL32_IP       : mysqlslave3