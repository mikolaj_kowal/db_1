#!/bin/bash

mysql --host mysqlslave --port=3306 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e 'STOP SLAVE;';
mysql --host mysqlslave2 --port=3307 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e 'STOP SLAVE;';
mysql --host mysqlslave3 --port=3308 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e 'STOP SLAVE;';

MYSQL01_Position=$(eval "mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -e 'show master status \G' | grep Position | sed -n -e 's/^.*: //p'")
MYSQL01_File=$(eval "mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -e 'show master status \G'     | grep File     | sed -n -e 's/^.*: //p'")

MYSQL21_Position=$(eval "mysql --host mysqlmaster2 --port=3307 -uroot -p$MYSQL_MASTER_PASSWORD -e 'show master status \G' | grep Position | sed -n -e 's/^.*: //p'")
MYSQL21_File=$(eval "mysql --host mysqlmaster2 --port=3307 -uroot -p$MYSQL_MASTER_PASSWORD -e 'show master status \G'     | grep File     | sed -n -e 's/^.*: //p'")

MYSQL31_Position=$(eval "mysql --host mysqlmaster3 --port=3308 -uroot -p$MYSQL_MASTER_PASSWORD -e 'show master status \G' | grep Position | sed -n -e 's/^.*: //p'")
MYSQL31_File=$(eval "mysql --host mysqlmaster3 --port=3308 -uroot -p$MYSQL_MASTER_PASSWORD -e 'show master status \G'     | grep File     | sed -n -e 's/^.*: //p'")

mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -AN -e "CHANGE MASTER TO master_host='mysqlmaster2', master_port=3307, \
        master_user='$MYSQL_REPLICATION_USER', master_password='$MYSQL_REPLICATION_PASSWORD', master_log_file='$MYSQL21_File', \
        master_log_pos=$MYSQL21_Position;"

mysql --host mysqlmaster2 -uroot -p$MYSQL_MASTER_PASSWORD -AN -e "CHANGE MASTER TO master_host='mysqlmaster', master_port=3306, \
        master_user='$MYSQL_REPLICATION_USER', master_password='$MYSQL_REPLICATION_PASSWORD', master_log_file='$MYSQL01_File', \
        master_log_pos=$MYSQL21_Position;"

mysql --host mysqlslave --port=3306 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e 'START SLAVE;';
mysql --host mysqlslave2 --port=3307 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e 'START SLAVE;';
mysql --host mysqlslave3 --port=3308 -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e 'START SLAVE;';